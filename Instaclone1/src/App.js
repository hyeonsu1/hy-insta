import React from 'react';
import {SafeAreaView} from 'react-native';
import MainNavigation from './navigator/MainNavigation';

export default function App() {
  return <MainNavigation />;
}
