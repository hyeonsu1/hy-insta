import React from 'react';
import {Image, SafeAreaView} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../screens/HomeScreen';
import Search from '../screens/SearchScreen';
import Reels from '../screens/ReelsScreen';
import Shop from '../screens/ShopScreen';
import Profile from '../screens/ProfileScreen';

const Tab = createBottomTabNavigator();

export default function MainNavigation() {
  return (
    <NavigationContainer>
      {/*
      tabBarOptions : 탭 바 옵션 설정하는 부분 (디플리케이트 될 옵션이라 걷어내야함.)
      screenOptions : 아이콘 관련 옵션 설정하는 부분
      Tab.Screen : 컴포넌트 연결하는 부분
      */}
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused}) => {
            let iconName;
            if (route.name === 'Home') {
              iconName = focused
                ? require('../assets/images/home_icon.png')
                : require('../assets/images/home_icon.png');
            } else if (route.name === 'Search') {
              iconName = focused
                ? require('../assets/images/search_icon.png')
                : require('../assets/images/search_icon.png');
            } else if (route.name === 'Reels') {
              iconName = focused
                ? require('../assets/images/reels_icon.png')
                : require('../assets/images/reels_icon.png');
            } else if (route.name === 'Shop') {
              iconName = focused
                ? require('../assets/images/shop_icon.png')
                : require('../assets/images/shop_icon.png');
            } else if (route.name === 'Profile') {
              iconName = focused
                ? require('../assets/images/profile_icon.png')
                : require('../assets/images/profile_icon.png');
            }
            return <Image source={iconName} style={{width: 25, height: 25}} />;
          },
          tabBarShowLabel: 'false',
        })}>
        <Tab.Screen
          name="Home"
          component={Home}
          options={{headerShown: false}}
        />
        <Tab.Screen
          name="Search"
          component={Search}
          options={{headerShown: false}}
        />
        <Tab.Screen
          name="Reels"
          component={Reels}
          options={{headerShown: false}}
        />
        <Tab.Screen
          name="Shop"
          component={Shop}
          options={{headerShown: false}}
        />
        <Tab.Screen
          name="Profile"
          component={Profile}
          options={{headerShown: false}}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
