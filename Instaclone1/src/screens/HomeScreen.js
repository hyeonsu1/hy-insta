import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';

// TODO : 스토리 flatlist로 구현

{
  /* DATA: Flatlist의 아이템에 들어갈 데이터 */
}
const DATA = [
  {
    id: '1',
    title: 'First Item',
  },
  {
    id: '2',
    title: 'Second Item',
  },
  {
    id: '3',
    title: 'Third Item',
  },
  {
    id: '4',
    title: 'Forth Item',
  },
  {
    id: '5',
    title: 'Fifth Item',
  },
  {
    id: '6',
    title: 'Sixth Item',
  },
  {
    id: '7',
    title: 'Seventh Item',
  },
  {
    id: '8',
    title: 'Eighth Item',
  },
  {
    id: '9',
    title: 'Ninth Item',
  },
  {
    id: '10',
    title: 'Tenth Item',
  },
];

// 플랫리스트 아이템 구현부
const Item = ({item, onPress, style}) => (
  <TouchableOpacity onPress={onPress} style={[styles.item, style]}>
    <Text style={styles.title}>{item.title}</Text>
  </TouchableOpacity>

  // TODO : 피드 아이템 레이아웃 구현
  // 글쓴이 정보 표시 부분
  // 사진 viewholder 부분
  // 좋아요 등 버튼과 뷰홀더 네비게이션바 부분
  // 좋아요 갯수 표시 부분
  // 작성글과 일부 댓글 표시 부분
  // 댓글 갯수 표시와 댓글 전체보기 버튼 부분
  // 댓글 입력창 부분
  // 글 작성 시간 표시 부분
);

// 플랫리스트 구현부
function Search() {
  // State값 삽입
  const [selectedId, setSelectedId] = useState(null);

  // 아이템에 데이터 넣고 이벤트 지정해주는 부분
  const renderItem = ({item}) => {
    // id가 selectedId라면 배경색상 변경
    const backgroundColor = item.id === selectedId ? '#6e3b6e' : '#f9c2ff';

    return (
      <Item
        item={item}
        // 아이템을 클릭하면 selectedId가 변경
        onPress={() => setSelectedId(item.id)}
        style={{backgroundColor}}
      />
    );
  };

  // 목록 부분
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        // 데이터 담아서 아이디 값 순서대로 출력하는 속성
        data={DATA}
        // 리스트 내 아이템 레이아웃 설정하는 속성
        renderItem={renderItem}
        // 각각의 item의 고유의 키를 자동으로 부여하는 속성
        keyExtractor={item => item.id}
        // selectedId가 변경되면 리렌더링 되도록 하는 속성
        extraData={selectedId}
      />
    </SafeAreaView>
  );
}

export default function Home() {
  return (
    <SafeAreaView>
      {/* View부터 상단 바 부분 */}
      <View style={styles.BackgroundTopBar}>
        <Image
          style={styles.InstaLogoTopBar}
          source={require('../assets/images/Instafont_logo.png')}
        />
        <View style={styles.SpaceTopBar} />
        <Image
          style={styles.IconTopBar}
          source={require('../assets/images/plus_icon.png')}
        />
        <Image
          style={styles.IconTopBar}
          source={require('../assets/images/like_icon.png')}
        />
        <Image
          style={styles.IconTopBar}
          source={require('../assets/images/InstaSend_icon.png')}
        />
      </View>
      {/* ScrollView부터 피드 나타나는 flatlist 부분 */}
      <ScrollView>
        <Search />
      </ScrollView>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  BackgroundTopBar: {
    height: 65,
    alignContent: 'center',
    flexDirection: 'row',
  },
  SpaceTopBar: {
    flex: 2,
  },
  InstaLogoTopBar: {
    width: 150,
    height: 50,
    margin: 10,
    resizeMode: 'contain',
  },
  IconTopBar: {
    width: 50,
    height: 50,
    margin: 5,
    alignContent: 'center',
    resizeMode: 'contain',
  },
  BackgroundFeed: {
    height: 5000,
    alignContent: 'center',
    backgroundColor: 'blue',
    flexDirection: 'row',
  },
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});
