import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Image,
  StyleSheet,
  StatusBar,
} from 'react-native';

export default function Profile() {
  return (
    <SafeAreaView>
      {/* 상단 바 부분 */}
      <View style={styles.BackgroundTopBar}>
        <Image
          style={styles.InstaLogoTopBar}
          source={require('../assets/images/Instafont_logo.png')}
        />
        <View style={styles.SpaceTopBar} />
        <Image
          style={styles.IconTopBar}
          source={require('../assets/images/plus_icon.png')}
        />
        <Image
          style={styles.IconTopBar}
          source={require('../assets/images/menu_icon.png')}
        />
      </View>

      {/* 프로필 정보 부분 */}
      {/* 프로필 설명 부분 */}
      {/* 프로필 버튼 부분 */}
      {/* 탭 부분 */}
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  BackgroundTopBar: {
    height: 65,
    alignContent: 'center',
    flexDirection: 'row',
  },
  SpaceTopBar: {
    flex: 2,
  },
  InstaLogoTopBar: {
    width: 150,
    height: 50,
    margin: 10,
    resizeMode: 'contain',
  },
  IconTopBar: {
    width: 50,
    height: 50,
    margin: 5,
    alignContent: 'center',
    resizeMode: 'contain',
  },
});
